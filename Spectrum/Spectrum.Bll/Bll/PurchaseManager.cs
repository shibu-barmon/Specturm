﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Spectrum.Model.Model;
using Spectrum.Repository.Repository;

namespace Spectrum.Bll.Bll
{
   public class PurchaseManager
    {
        PurchaseRepository _purchaseRepository = new PurchaseRepository();

        public bool Add(Purchase purchase)
        {
            return _purchaseRepository.Add(purchase);
        }

        public bool Delete(int id)
        {
            return _purchaseRepository.Delete(id);
        }
        public List<Purchase> GetAll()
        {
            return _purchaseRepository.GetAll();
        }
        public Purchase getById(int id)
        {
            return _purchaseRepository.getById(id);
        }
    }
}
